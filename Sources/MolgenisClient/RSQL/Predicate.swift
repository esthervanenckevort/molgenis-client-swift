//
//  Predicate.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 08-05-18.
//
import Foundation

public enum Predicate {
    case and(predicates: [Predicate])
    case or(predicates: [Predicate])
    case equal(subject: Attribute, object: Any)
    case notEqual(subject: Attribute, object: Any)
    case lessThan(subject: Attribute, object: Any)
    case greaterThan(subject: Attribute, object: Any)
    case lessOrEqualTo(subject: Attribute, object: Any)
    case greaterOrEqualTo(subject: Attribute, object: Any)
    case containedIn(subject: Attribute, object: Any)
    case notContainedIn(subject: Attribute, object: Any)
    case similarTo(subject: Attribute, object: Any)
}
