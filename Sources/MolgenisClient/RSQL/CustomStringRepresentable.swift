//
//  CustomStringRepresentable.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 09-05-18.
//

import Foundation

extension Predicate: CustomStringConvertible {
    public var description: String {
        switch self {
        case .equal(let subject, let object):
            return "\(subject) = \(object)"
        case .notEqual(let subject, let object):
            return "\(subject) ≠ \(object)"
        case .lessThan(let subject, let object):
            return "\(subject) < \(object)"
        case .greaterThan(let subject, let object):
            return "\(subject) > \(object)"
        case .lessOrEqualTo(let subject, let object):
            return "\(subject) ≤ \(object)"
        case .greaterOrEqualTo(let subject, let object):
            return "\(subject) ≥ \(object)"
        case .containedIn(let subject, let object):
            return "\(subject) ∈ {\(object)}"
        case .notContainedIn(let subject, let object):
            return "\(subject) ∉ {\(object)}"
        case .similarTo(let subject, let object):
            return "\(subject) ~ \(object)"
        case .and(let predicates):
            return "(\(predicates.map { $0.description }.joined(separator: " & "))"
        case .or(let predicates):
            return "(\(predicates.map { $0.description }.joined(separator: " | "))"
        }
    }


}
