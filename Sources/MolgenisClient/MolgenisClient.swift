//
//  Molgenis.swift
//  Molgenis
//
//  Created by David van Enckevort on 08-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
import Promise
import os

public class MolgenisClient {
    static private let MIN_VERSION = Version(version: "1.15.0")
    private let server: URLComponents
    private var loginResult: LoginResult?
    private var session: URLSession
    private lazy var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        return encoder
    }()
    private let worker = DispatchQueue.global(qos: .utility)
    public var isLoggedIn: Bool {
        return loginResult != nil
    }
    public var loggedInUser: LoginResult? {
        return loginResult
    }
    
    private init(url: URLComponents) {
        let configuration = URLSessionConfiguration.default
        session = URLSession(configuration: configuration)
        server = url
    }

    public static func make(url: URL) -> Promise<MolgenisClient> {
        let versionURL = url.appendingPathComponent("/api/v2/version")
        let worker = DispatchQueue.global(qos: .utility)
        return URLSession.shared.dataTask(with: versionURL)
            .then(on: worker) { (data, response) in
                return try Version.make(json: data).unwrap()
            }.ensure { (version) in
                return version >= MolgenisClient.MIN_VERSION
            }.then(on: worker) { (version) in
                return try URLComponents(url: url, resolvingAgainstBaseURL: false).unwrap()
            }.then(on: worker) { (url) in
                return MolgenisClient(url: url)
        }
    }

    private func POST<T: Encodable>(path: String, with encodable: T) throws -> URLRequest? {
        guard var request = makeRequest(path: "/api/v1/login") else { return nil }
        request.httpBody = try encoder.encode(encodable)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }

    public func login(username: String, password: String) -> Promise<MolgenisClient> {
        return Promises
            .kickoff {
                return try self.POST(path: "/api/v1/login", with: LoginRequest(username: username, password: password)).unwrap()
            }.then(on: worker) { (request) in
                return self.session.dataTask(with: request)
            }.ensure { (_, response) -> Bool in
                return response.statusCode == 200
            }.then(on: worker) { (data, response) throws -> MolgenisClient in
                self.loginResult = try LoginResult(json: data).unwrap()
                return self
            }.catch(on: worker) { (error) in
                OSLog.network.debug("Login failed with error %@.", params: error)
        }

    }

    public func entities() -> Promise<[EntityType]> {
        let entityType = EntityType("/api/v2/sys_md_EntityType", id: "id")
        return collection(of: entityType)
            .then(on: worker) { (collection) in
                return collection.map { return EntityType(URL(string: "/api/v2/\($0.id)")!, id: $0.metadata.idAttribute) }
            }
    }

    public func entity(with id: String, of type: EntityType) -> Promise<Entity> {
        return Promises
            .kickoff { () -> URLRequest in
                let path = type.href.appendingPathComponent(id).path
                return try self.makeRequest(path: path).unwrap()
            }.then(on: worker) { (request) in
                return self.session.dataTask(with: request)
            }.then(on: worker) { (data, response) in
                return try Entity(json: data).unwrap()
            }.catch(on: worker) { (error) in
                OSLog.network.debug("Failed to get entity of type %@ and id %@: %@", params: type, id, error)
        }
    }

    public func collection(of type: EntityType, search criteria: Predicate? = nil) -> Promise<EntityCollection> {
        return Promises.kickoff { [unowned self] in
            return try self.makeRequest(path: type.href.path, criteria: criteria).unwrap()
            }.then(on: worker) { (request) in
                return self.loadData(for: request, into: EntityCollection(of: type))
            }.catch(on: worker) { (error) in
                OSLog.network.debug("Failed to load collection of type %@, error %@.", params: type, error)
        }
    }

    private func loadData(for request: URLRequest, into container: EntityCollection) -> Promise<EntityCollection> {
        return session.dataTask(with: request)
            .then(on: worker) { (data, response) in
                return try EntityCollection(json: data).unwrap()
            }.then(on: worker) { (result) in
                let collected = container + result
                if let next = result.nextHref {
                    let request =  self.makeRequest(url: next)
                    OSLog.network.debug("Loading data for %@.", params: next)
                    return self.loadData(for: request, into: collected)
                } else {
                    OSLog.network.debug("Completed loading data for %@ with %@ items.", params: collected.href, collected.count)
                    let promise = Promise<EntityCollection>()
                    promise.fulfill(collected)
                    return promise
                }
            }.catch(on: worker) { (error) in
                OSLog.network.debug("Failed to load data for %@ with error %@.", params: request, error)
        }
    }

    private func makeRequest(path: String, criteria: Predicate? = nil) -> URLRequest? {
        var components = server
        components.path = path
        if let criteria = criteria {
            let query = URLQueryItem(name: "q", value: criteria.rsql)
            components.queryItems = [query]
        }
        guard let requestURL = components.url else { return nil }
        return makeRequest(url: requestURL)
    }

    private func makeRequest(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        if let token = loginResult?.token {
            request.addValue(token, forHTTPHeaderField: "x-molgenis-token")
        }
        OSLog.network.debug("request %@ intialized.", params: request.debugDescription)
        return request
    }

    enum MolgenisError: Error {
        case unknown
        case invalidURL
        case incompatibleServerVersion
        case initialisationError
        case decodingError
        case noData
        case loginFailed
    }
}

private extension URLComponents {
    init?(url: URL, path: String) {
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else { return nil }
        components.path = path
        self = components
    }
}
private extension Array {
    func appending<Other: Sequence>(contentsOf sequence: Other) -> [Element] where Other.Element == Element {
        var array = self
        array.append(contentsOf: sequence)
        return array
    }
}
