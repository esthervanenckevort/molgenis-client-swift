//
//  LoginResult.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation

public struct LoginResult: Decodable {
    var token: String
    public var username: String
    public var firstname: String?
    public var lastname: String?
}

extension LoginResult {
    init?(json data: Data) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        if let result = try? decoder.decode(LoginResult.self, from: data) {
            self = result
        } else {
            return nil
        }
    }
}
