//
//  EntityCache.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 12-05-18.
//

import Foundation
import Promise

class EntityCache {
    private let client: MolgenisClient
    private let type: EntityType
    private var cache: [String:Entity] = [:]

    init(for type: EntityType, client: MolgenisClient) {
        self.type = type
        self.client = client
    }

    func get(with id: String) -> Promise<Entity> {
        let promise = Promise<Entity>()
        if let value = cache[id] {
            promise.fulfill(value)
            return promise
        }
        return client.entity(with: id, of: type).then { [weak self] (entity) -> Entity in
            self?.cache[id] = entity
            return entity
        }
    }
}
