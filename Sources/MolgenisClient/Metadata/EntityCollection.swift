//
//  EntityContainer.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation
import os

public struct EntityCollection: Decodable, Collection {
    public typealias Element = Entity
    public typealias Index = Int

    public func index(after i: Int) -> Int {
        let next = i + 1
        return next < endIndex ? next : endIndex
    }

    public subscript(position: Int) -> Entity {
        guard let entity = Entity(metadata: meta, data: items[position]) else {
            fatalError("Failed to initialize entity: \(href) \(position)")
        }
        return entity
    }

    public var startIndex: Int {
        return 0
    }

    public var endIndex: Int {
        return count
    }

    public static func +(lhs: EntityCollection, rhs: EntityCollection) -> EntityCollection {
        var collection = lhs
        collection.items = lhs.items + rhs.items
        collection.prevHref = min(lhs.prevHref, rhs.prevHref)
        collection.nextHref = max(lhs.nextHref, rhs.nextHref)
        return collection
    }

    private static func min(_ lhs: URL?, _ rhs: URL?) -> URL? {
        switch (lhs, rhs) {
        case (.none, .some(let url)):
            return url
        case (.some(let url), .none):
            return url
        case (.some(let left), .some(let right)):
            return left.absoluteString > right.absoluteString ? left : right
        default: return nil
        }
    }

    private static func max(_ lhs: URL?, _ rhs: URL?) -> URL? {
        switch (lhs, rhs) {
        case (.none, .some(let url)):
            return url
        case (.some(let url), .none):
            return url
        case (.some(let left), .some(let right)):
            return left.absoluteString > right.absoluteString ? left : right
        default: return nil
        }
    }

    public init(of type: EntityType) {
        meta = type
        href = meta.href
        items = []
    }

    public private(set) var meta: EntityType
    public var count: Int {
        return items.count
    }
    public private(set) var nextHref: URL?
    public private(set) var prevHref: URL?
    public private(set) var href: URL
    public private(set) var items: [[String:Any]] = []

    private enum CodingKeys: String, CodingKey {
        case meta, nextHref, prevHref, href
    }
}


extension EntityCollection {
    init?(json data: Data) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
            self = try decoder.decode(EntityCollection.self, from: data)
            let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            if let dict = dict, let items = dict["items"] as? [[String:Any]] {
                self.items = items
            }
        } catch let error {
            os_log("Failed to deserialize data: %s", log: .data, type: .debug, error.localizedDescription)
            return nil
        }
    }
}
