//
//  EntityType.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation

public struct EntityType: Codable {
    public private(set) var href: URL
    public private(set) var hrefCollection: URL?
    public private(set) var name: String?
    public var label: String?
    public var description: String?
    public private(set) var attributes: [Attribute]?
    public var labelAttribute: String?
    public var idAttribute: String
    public var lookupAttributes: [String]?
    public var isAbstract: Bool?
    public var writable: Bool?
    public var languageCode: String?
    public var allAttributes: [Attribute] {
        return flattened(attributes)
    }

    private func flattened(_ attributes: [Attribute]?) -> [Attribute] {
        guard let attributes = attributes else { return [] }
        var list = [Attribute]()
        for attribute in attributes {
            list.append(attribute)
            if attribute.fieldType == .COMPOUND {
                list.append(contentsOf: flattened(attribute.attributes))
            }
        }
        return list
    }
}

extension EntityType {
    init(_ url: URL, id idAttribute: String) {
        self = EntityType(href: url, hrefCollection: nil, name: nil, label: nil, description: nil, attributes: nil, labelAttribute: nil, idAttribute: idAttribute, lookupAttributes: nil, isAbstract: nil, writable: nil, languageCode: nil)
    }
}
