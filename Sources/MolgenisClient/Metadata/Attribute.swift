//
//  Attribute.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation

public struct Attribute: Codable {
    public var href: URL
    public var fieldType: FieldType
    public var name: String
    public var label: String?
    public var description: String?
    public var attributes: [Attribute]
    public var refEntity: EntityType?
    public var maxLength: Int?
    public var auto: Bool?
    public var nillable: Bool
    public var readOnly: Bool
    public var labelAttribute: Bool
    public var unique: Bool
    public var visible: Bool
    public var lookupAttribute: Bool
    public var isAggregatable: Bool
    public var enumOptions: [String]?
    public var rangeMin: Int?
    public var rangeMax: Int?
}
