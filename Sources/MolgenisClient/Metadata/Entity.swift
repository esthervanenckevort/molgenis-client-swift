//
//  Entity.swift
//  Molgenis
//
//  Created by David van Enckevort on 24-06-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
import os

public struct Entity: Decodable {
    public private(set) var href: URL
    public private(set) var metadata: EntityType
    public private(set) var data: [String:Any] = [:]

    public init?(metadata: EntityType, data: [String:Any]) {
        guard let value = data[metadata.idAttribute] else {
            OSLog.data.debug("Failed to get data for id attribute %@", params: metadata.idAttribute)
            return nil
        }
        let id = String(describing: value)
        self.metadata = metadata
        self.data = data
        self.href = metadata.href.appendingPathComponent(id)
    }

    private enum CodingKeys: String, CodingKey {
        case href = "_href", metadata = "_meta"
    }

    public subscript(string name: String) -> Any? {
        return data[name]
    }
    
    public var id: String {
        let value = self[string: metadata.idAttribute] ?? ""
        return String(describing: value)
    }
    
    public var label: String? {
        guard let labelAttribute = metadata.labelAttribute else { return nil }
        return self[string: labelAttribute] as? String
    }
  
    public func attribute(for name: String) -> Attribute? {
        let attribute = metadata.allAttributes.filter { $0.name == name }
        return attribute.first
    }
}

extension Entity {

    init?(json data: Data) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
            self = try decoder.decode(Entity.self, from: data)
            if let data = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
                self.data = data
            }
        } catch {
            os_log("Failed to deserialize data: %s", log: .data, type: .debug, error.localizedDescription)
            return nil
        }
    }
}
