//
//  File.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation

public enum FieldType: String, Codable {
    case BOOL, CATEGORICAL, CATEGORICAL_MREF, COMPOUND,
         DATE, DATE_TIME, DECIMAL, EMAIL, ENUM, FILE, HTML,
         HYPERLINK, INT, LONG, MREF, ONE_TO_MANY, SCRIPT,
         STRING, TEXT, XREF
    public static let values = [BOOL, CATEGORICAL, CATEGORICAL_MREF, COMPOUND,
                         DATE, DATE_TIME, DECIMAL, EMAIL, ENUM, FILE, HTML,
                         HYPERLINK, INT, LONG, MREF, ONE_TO_MANY, SCRIPT,
                         STRING, TEXT, XREF]

    public static let text = [TEXT, STRING, EMAIL, HYPERLINK, HTML]
    public static let numeric = [INT, LONG, DECIMAL]
    public static let temporal = [DATE, DATE_TIME]
    public static let reference = [CATEGORICAL, CATEGORICAL_MREF, ONE_TO_MANY, MREF, XREF, ENUM]
    public static let data = [SCRIPT, FILE]
    public static let structural = [COMPOUND]
    public static let logical = [BOOL]
    /*
     Override for pattern matching to allow switch statements on the above defined groups
     */
    static func ~=(pattern: [FieldType], value: FieldType) -> Bool {
        return pattern.contains(value)
    }
}
