//
//  Promise.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 07-07-18.
//

import Foundation
import Promise
import os

extension URLSession {
    func dataTask(with url: URL) -> Promise<(Data, HTTPURLResponse)> {
        return dataTask(with: URLRequest(url: url))
    }

    func dataTask(with request: URLRequest) -> Promise<(Data, HTTPURLResponse)> {
        return Promise<(Data, HTTPURLResponse)>() { fulfill, reject in
            let task = self.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    reject(error)
                } else if let data = data, let response = response as? HTTPURLResponse {
                    OSLog.network.debug("Fullfilled request %@.", params: request.debugDescription)
                    fulfill((data, response))
                } else {
                    fatalError("Fatal error while initializing MOLGENIS Client.")
                }
            }
            task.resume()
        }
    }
}
