//
//  Optional.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 07-07-18.
//

import Foundation

struct NilError: Error { }

extension Optional {
    func unwrap() throws -> Wrapped {
        guard let result = self else {
            throw NilError()
        }
        return result
    }
}
