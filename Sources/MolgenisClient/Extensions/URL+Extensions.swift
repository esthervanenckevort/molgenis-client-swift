//
//  URL+Extensions.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 06-07-18.
//

import Foundation

extension URL: ExpressibleByStringLiteral {
    public typealias StringLiteralType = String

    public init(stringLiteral value: URL.StringLiteralType) {
        self = URL(string: value)!
    }
}
