//
//  Logging.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 06-05-18.
//

import Foundation
import os

extension OSLog {
    static var data = OSLog(subsystem: "MolgenisClient", category: "data")
    static var network = OSLog(subsystem: "MolgenisClient", category: "network")

    func debug(_ message: StaticString, params: Any...) {
        os_log(message, log: self, type: .debug, params)
    }

    func info(_ message: StaticString, params: Any...) {
        os_log(message, log: self, type: .info, params)
    }

    func error(_ message: StaticString, params: Any...) {
        os_log(message, log: self, type: .error, params)
    }

    func fault(_ message: StaticString, params: Any...) {
        os_log(message, log: self, type: .fault, params)
    }

    func log(_ message: StaticString, params: Any...) {
        os_log(message, log: self, type: .default, params)
    }


}
