//
//  MolgenisVersion.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation
import os

struct Version: Decodable, Comparable {
    private var version: String
    private var buildDate: Date?

    private enum CodingKeys: String, CodingKey {
        case version = "molgenisVersion", buildDate
    }

    static func <(lhs: Version, rhs: Version) -> Bool {
        let (lhsMajor, lhsMinor, lhsRevision, _) = lhs.parseVersion()
        let (rhsMajor, rhsMinor, rhsRevision, _) = rhs.parseVersion()
        if rhsMajor > lhsMajor {
            return true
        }
        if rhsMajor == lhsMajor && rhsMinor > lhsMinor {
            return true
        }
        if lhsMajor == lhsMajor && rhsMinor == rhsMinor && rhsRevision > lhsRevision {
            return true
        }
        return false
    }

    static func ==(lhs: Version, rhs: Version) -> Bool {
        let (lhsMajor, lhsMinor, lhsRevision, lhsModifier) = lhs.parseVersion()
        let (rhsMajor, rhsMinor, rhsRevision, rhsModifier) = rhs.parseVersion()
        return lhsMajor == rhsMajor && lhsMinor == rhsMinor && lhsRevision == rhsRevision && lhsModifier == rhsModifier
    }

    private func parseVersion() -> (major: Int, minor: Int, Revsion: Int, modifier: String?) {
        let components = version.components(separatedBy: ".")
        let major = Int(components[0]);
        let minor = Int(components[1]);
        if components.count == 3 {
            let parts = components[2].components(separatedBy: "-")
            var modifier: String?
            if parts.count == 2 {
                modifier = parts[1]
            }
            let revision = Int(parts[0]);
            return (major ?? 0, minor ?? 0, revision ?? 0, modifier);
        }
        return (major ?? 0, minor ?? 0, 0, nil)
    }

    var major: Int {
        let (major, _, _, _) = parseVersion()
        return major
    }

    var minor: Int {
        let (_, minor, _, _) = parseVersion()
        return minor
    }

    var revision: Int {
        let (_, _, revision, _) = parseVersion()
        return revision
    }

    var modifier: String? {
        let (_, _, _, modifier) = parseVersion()
        return modifier
    }

    init(version: String) {
        self.version = version
    }

    static func make(json data: Data) -> Version? {
        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd HH:mm ZZZZZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        decoder.dateDecodingStrategy = .formatted(formatter)
        do {
            return try decoder.decode(Version.self, from: data)
        } catch {
            os_log("Failed to deserialize data: %s", log: .data , type: .debug, error.localizedDescription)
            return nil
        }
    }
}
