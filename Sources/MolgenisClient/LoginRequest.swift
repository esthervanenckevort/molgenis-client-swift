//
//  LoginRequest.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 04-05-18.
//

import Foundation

struct LoginRequest: Encodable {
    var username: String
    var password: String
}
