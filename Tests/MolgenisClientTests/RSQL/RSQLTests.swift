//
//  RSQLTests.swift
//  MolgenisClientTests
//
//  Created by David van Enckevort on 09-05-18.
//

import XCTest
@testable import MolgenisClient

class RSQLTests: XCTestCase {

    func testExample() {
        let attribute = Attribute(href: URL(string: "/bla")!, fieldType: .BOOL, name: "bla", label: "bla", description: "bla", attributes: [], refEntity: nil, maxLength: nil, auto: true, nillable: false, readOnly: true, labelAttribute: true, unique: true, visible: false, lookupAttribute: true, isAggregatable: true, enumOptions: nil, rangeMin: nil, rangeMax: nil)
        let item1 = Predicate.equal(subject: attribute, object: true)
        let item2 = Predicate.greaterOrEqualTo(subject: attribute, object: false)
        let and = Predicate.and(predicates: [item1, item2])
        XCTAssert(and.description.count > 0)
        XCTAssert(and.rsql.count > 0)
        XCTAssertEqual(and.rsql, "bla==true;bla=ge=false")
    }
}
