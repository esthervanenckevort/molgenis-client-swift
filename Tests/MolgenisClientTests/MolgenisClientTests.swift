import XCTest
@testable import MolgenisClient
import Promise

final class MolgenisClientTests: XCTestCase {
    var config: TestConfiguration = Config()

    func testMake() {
        let server = config.testInstance
        let expectation = XCTestExpectation(description: "Created a molgenis client")
        MolgenisClient.make(url: server.url)
            .then { (client) in
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 30.0)
    }

    func testMakeWithInvalidUrl() {
        let server = config.invalidInstance
        let expectation = XCTestExpectation(description: "Failed to molgenis client")
        MolgenisClient.make(url: server.url)
            .catch { (error) in
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 30.0)
    }

    func testLogin() {
        let server = config.testInstance
        let expectation = XCTestExpectation(description: "Login to MOLGENIS")
        let result = MolgenisClient.make(url: server.url)
            .then { (client) in
                return client.login(username: server.username, password: server.password)
            }
            .ensure { (client) in
                return client.isLoggedIn
            }.always {
                expectation.fulfill()
                }.catch { (error) in
                    XCTFail(error.localizedDescription)
        }
        wait(for: [expectation], timeout: 30.0)
        XCTAssert(result.value!.isLoggedIn)
        XCTAssertTrue(result.value!.loggedInUser!.username == "admin")
    }

    func testLoginWithWrongPassword() {
        let server = config.invalidPasswordInstance
        let expectation = XCTestExpectation(description: "Login to MOLGENIS")
        let result = MolgenisClient.make(url: server.url)
            .then { (client) in
                return client.login(username: server.username, password: server.password)
            }.catch { error in
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 30.0)
        XCTAssertTrue(result.isRejected)
    }

    func testLoginWithWrongUser() {
        let server = config.invalidUsernameInstance
        let expectation = XCTestExpectation(description: "Login to MOLGENIS")
        let result = MolgenisClient.make(url: server.url)
            .then { (client) in
                return client.login(username: server.username, password: server.password)
            }.catch { error in
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 30.0)
        XCTAssertTrue(result.isRejected)
    }


    func testCollection() {
        let url = URL(string: "/api/v2/sys_md_EntityType")!
        let type = EntityType(url, id: "id")
        let expectation = XCTestExpectation(description: "Get collection")
        let client = molgenis(with: config.testInstance)
        let result = client
            .then { (client) in
                return client.collection(of: type)
            }.always {
                expectation.fulfill()
            }.catch { (error) in
                XCTFail(error.localizedDescription)
        }
        wait(for: [expectation], timeout: 30.0)
        XCTAssertNotNil(result.value)
        XCTAssert(result.value!.count == 152)
    }

    private func molgenis(with config: MolgenisConfiguration) -> Promise<MolgenisClient> {
        let expectation = XCTestExpectation(description: #function)
        let molgenis = MolgenisClient.make(url: config.url)
            .then { (client) in
                return client.login(username: config.username, password: config.password)
            }.always {
                expectation.fulfill()
            }.catch { (error) in
                XCTFail(error.localizedDescription)
        }
        wait(for: [expectation], timeout: 30)
        return molgenis
    }

    func testGetEntity() {
        let url = URL(string: "/api/v2/sys_md_EntityType")!
        let type = EntityType(url, id: "id")
        let expectation = XCTestExpectation(description: #function)
        let client = molgenis(with: config.testInstance)
        let result = client
            .then { (client) in
                return client.entity(with: "sys_FileMeta", of: type)
            }.always {
                expectation.fulfill()
            }.catch { (error) in
                XCTFail(error.localizedDescription)
        }
        wait(for: [expectation], timeout: 30.0)
        XCTAssertTrue(result.isFulfilled)
        XCTAssertNotNil(result.value)
        XCTAssert(result.value!.label == "File metadata")
        XCTAssert(result.value!.id == "sys_FileMeta")
    }

    func testGetEntities() {
        let expectation = XCTestExpectation(description: #function)
        let client = molgenis(with: config.testInstance)
        let result = client
            .then { (client) in
                return client.entities()
            }.always {
                expectation.fulfill()
            }.catch { (error) in
                XCTFail(error.localizedDescription)
        }
        wait(for: [expectation], timeout: 30.0)
        XCTAssertNotNil(result.value)
        guard let types = result.value else {
            XCTFail("Result was nil")
            return
        }
        let urls = types.map { $0.href }
        XCTAssertTrue(urls.contains("/api/v2/sys_md_EntityType"))
    }
}
