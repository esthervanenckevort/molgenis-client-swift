//
//  MolgenisConfiguration.swift
//  MolgenisClient
//
//  Created by David van Enckevort on 06-07-18.
//

import Foundation
import os

struct MolgenisConfiguration: Codable {
    var url: URL
    var username: String
    var password: String
}
