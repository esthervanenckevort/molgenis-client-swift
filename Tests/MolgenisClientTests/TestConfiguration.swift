//
//  Configuration.swift
//  MolgenisClientTests
//
//  Created by David van Enckevort on 06-07-18.
//

import Foundation

protocol TestConfiguration {
    var testInstance: MolgenisConfiguration { get }
    var invalidInstance: MolgenisConfiguration { get }
    var invalidUsernameInstance: MolgenisConfiguration { get }
    var invalidPasswordInstance: MolgenisConfiguration { get }
}
